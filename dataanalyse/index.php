<?php

require("docheader.inc");
require("kdb.inc");
require("validcode.inc");
require("getvar.inc");

$get=new GetVars();
$showall=$get->add("showall",null,0);

$dbh=MyDatabase::connect("kageliste");

$header=new DocHeader();
$header->set_title("Dataanalyse kage og morgenmadsliste");
$header->set_css("/kage.css");
$header->set_javascript("http://www.ougar.dk/javascript/jquery-1.7.1.min.js");
$header->add_javascript("/kage.js");

if ($showall=="0") $dwhere="datediff(date,current_date()) between -18 and 70";
else $dwhere=1;

// Find all events to display
$next="if(@n=0 and date>=current_date() and datediff(date,current_date())<=7,@n:=1,0) as next";
$date_dk="concat(dayname_dk(date),' ',day(date),'. ',monthname_dk(date)) as date_dk";
try {
  $dbh->kquery("set @n:=0");
	$kageres=$dbh->kquery("select q.*,$next,$date_dk from (select * from events natural join users where event='kage' and $dwhere order by date) as q order by date");
  $dbh->kquery("set @n:=0");
	$madres=$dbh->kquery("select q.*,$next,$date_dk from (select * from events natural join users where event='morgenmad' and $dwhere order by date) as q order by date");
} catch (Exception $e) {
  print("<h1> Database error: </h1>\n");
	print($e->getMessage());
	die();
}

// Find all people used to add new events
$people=$dbh->kquery("select uid,username from users");
// Show header
$header->display();

print("<body>\n\n");
print("<h1> Dataanalyse - Hvem tager med </h1>\n\n");

// Vis kagetabel
print("<div id='kageliste'>\n".
      "<h2> Kage til afdelingsmøde </h2>\n");
printtable($kageres);
print("</div>\n\n");

// Vis morgenmadstabel
print("<div id='morgenmadsliste'>\n".
      "<h2> Fredags-morgenmad </h2>\n");
printtable($madres);
print("</div>\n\n");

// Footer with validcode
print("<div id='validcode'>\n".
      Validcode::valid_document()."\n".
			"</div>\n".
      "</body>\n".
			"</html>\n");

function printtable($res) {
	print("  <table>\n".
				"    <thead>\n".
				"      <tr> <th> Dato <th> Person\n".
				"    </thead>\n".
				"    <tbody>\n");
	while ($row=$res->fetch_assoc()) {
	  $next=($row['next']?" class='next'":"");
		print("    <tr$next> <td> {$row['date_dk']} <td> {$row['username']}\n");
	}
	print("    </tbody>\n".
				"  </table>\n");
}





?>
