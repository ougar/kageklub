<?php

require("kdb.inc");

$dbh=MyDatabase::connect("kageliste");

$q1="select uid,username,email,id,date from vkage where datediff(date,current_date())=1";
$q2="select uid,username,email,id,date from vmorgenmad where datediff(date,current_date())=1";

$kageres=$dbh->try_single_row($q1);
$madres=$dbh->try_single_row($q2);

if ($kageres) mailnotify($kageres['email'],$kageres['username'],"kage");
if ($madres) mailnotify($madres['email'],$madres['username'],"morgenmad");

if (!$kageres && !$madres)
  print(date("Y-m-d H:i")." - Intet sendt\n");

function mailnotify($email, $name, $event) {
  #$email="kristian.hougaard@skat.dk";
	$subj ="Husk $event i morgen";
	$head ="From: Kagelisten<kage@ougar.dk>";
	$body="Hej $name\n\n".
	      "Husk at det er dig, der skal have $event med i morgen.\n\n".
				"Hvis du ikke kommer eller ikke kan tage med af en eller anden ".
				"årsag, så må du meget gerne lige sende en mail til ".
				"Kristian (kristian.hougaard@skat.dk). ".
				"Så kan vi nå at finde en afløser :-)\n\n".
				"mvh. Kagelisten (kage.ougar.dk/dataanalyse)\n";
	mail($email, $subj, $body, $head);
  print(date("Y-m-d H:i")." - Husk $event sendt til $name\n");
}

?>
