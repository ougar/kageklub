DROP FUNCTION if exists DAYNAME_DK;
DELIMITER //
CREATE FUNCTION DAYNAME_DK (input_date DATE)
RETURNS CHAR(8) DETERMINISTIC
BEGIN 
SET @old_lc_time_names = (SELECT VARIABLE_VALUE FROM information_schema.SESSION_VARIABLES WHERE VARIABLE_NAME = 'lc_time_names');
SET SESSION lc_time_names='da_DK'; 
SET SESSION character_set_results = 'utf8';
SET @day_dk = DAYNAME(input_date); 
SET SESSION lc_time_names = @old_lc_time_names;
RETURN @day_dk; 
END;
//
DELIMITER ;

DROP FUNCTION if exists MONTHNAME_DK;
DELIMITER //
CREATE FUNCTION MONTHNAME_DK (input_date DATE)
RETURNS CHAR(8) DETERMINISTIC
BEGIN 
SET @old_lc_time_names = (SELECT VARIABLE_VALUE FROM information_schema.SESSION_VARIABLES WHERE VARIABLE_NAME = 'lc_time_names');
SET SESSION lc_time_names='da_DK'; 
SET SESSION character_set_results = 'utf8';
SET @day_dk = MONTHNAME(input_date); 
SET SESSION lc_time_names = @old_lc_time_names;
RETURN @day_dk; 
END;
//
DELIMITER ;

DROP FUNCTION IF EXISTS ucfirst;
DELIMITER //
CREATE FUNCTION ucfirst(str_value VARCHAR(5000))
RETURNS VARCHAR(5000)
DETERMINISTIC
BEGIN
  RETURN CONCAT(UCASE(LEFT(str_value, 1)),SUBSTRING(str_value, 2));
END
//
