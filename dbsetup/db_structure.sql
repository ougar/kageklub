DROP TABLE IF EXISTS users;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE users (
  uid mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  username varchar(50) DEFAULT NULL,
  realname varchar(50) DEFAULT NULL,
  email varchar(50) DEFAULT NULL,
  password char(35) DEFAULT NULL,
  wnr smallint(6) DEFAULT NULL,
  birthday date DEFAULT NULL,
  birthtime time DEFAULT NULL,
  startday date DEFAULT NULL,
  superuser tinyint(1) DEFAULT '0',
  enabled tinyint(1) DEFAULT '1',
  inkage tinyint(1) DEFAULT '1',
  inmorgenmad tinyint(1) DEFAULT '1',
  hideemail tinyint(1) DEFAULT '0',
  visits int(10) unsigned DEFAULT '0',
  lastvisit timestamp NULL DEFAULT NULL,
  created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated timestamp NULL DEFAULT NULL,
  resetkey char(10) DEFAULT NULL,
  loginfail smallint(5) unsigned NOT NULL,
  lasttry timestamp NULL DEFAULT NULL,
  brutetime timestamp NULL DEFAULT NULL,
  brutecount smallint(6) DEFAULT NULL,
  PRIMARY KEY (uid),
  KEY uid_ind (uid)
) ENGINE=InnoDB;


DROP TABLE IF EXISTS events;
CREATE TABLE events (
  id int(11) NOT NULL AUTO_INCREMENT,
  uid mediumint(8) unsigned DEFAULT NULL,
  date date DEFAULT NULL,
  event enum('kage','morgenmad') DEFAULT NULL,
  PRIMARY KEY (id),
  KEY uid_ind (uid),
  CONSTRAINT events_ibfk_1 FOREIGN KEY (uid) REFERENCES users (uid)
) ENGINE=InnoDB;

create or replace view vkage AS select users.uid,username,email,id,date from users join events on users.uid = events.uid where event = 'kage' order by date;
create or replace view vmorgenmad AS select users.uid,username,email,id,date from users join events on users.uid = events.uid where event = 'morgenmad' order by date;
